Taller de Reducción de Imágenes Astronómicas
============================================

Taller en el marco del 
[Scientific And Technical Exchange Conferences (SATEC)](https://satec.iate.conicet.unc.edu.ar/),
organizado por el Instituto de Astronomía Teórica y Experimental (IATE)
conjuntamente con la Comisión Nacional de Actividades Espaciales (CONAE).

La charla que acompaña a este material está disponible en
[YouTube](https://youtu.be/ezJSvyhFfCE).


Dependencias
------------

Para quienes quieran utilizar su propia instalación, deben clonar el
repositorio e instalar las dependencias con `pip` o `conda` según les resulte
más cómodo a través del `requirements.txt` o `environment.yml`.


Sin instalación
---------------

* Se puede participar del taller desde cero accediendo aquí:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cvillalon%2Freduccion-de-imagenes-astronomicas/main?labpath=Taller.ipynb)

* Para ejecutar el notebook completo:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cvillalon%2Freduccion-de-imagenes-astronomicas/main?labpath=Taller%20completo.ipynb)
